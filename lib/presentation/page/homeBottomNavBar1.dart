import 'package:easybuy/data/models/categoryModel.dart';
import 'package:easybuy/presentation/page/BottomParPages/chat_page.dart';
import 'package:easybuy/presentation/page/BottomParPages/profile_page.dart';
import 'package:easybuy/presentation/page/BottomParPages/settings_page.dart';
import 'package:easybuy/presentation/widgets/AddNewProduct/ProductDetailsForm.dart';
import 'package:easybuy/presentation/widgets/bottomSheetBuilder.dart';
import 'package:easybuy/presentation/widgets/catgrid.dart';
import 'package:easybuy/presentation/widgets/filters.dart';
import 'package:easybuy/presentation/widgets/productList.dart';
import 'package:easybuy/presentation/widgets/searchBar.dart';
import 'package:easybuy/presentation/widgets/sideDrawer.dart';
import 'package:easybuy/presentation/widgets/AddNewProduct/AddNewProduct.dart';
import 'package:flutter/material.dart';

import 'BottomParPages/main_page.dart';
import 'co.dart';

class HomeBottomNavBar1 extends StatefulWidget {
  String list = '';
  static int currentTab;
  List<CatModel> catList = [];
  static GlobalKey<ScaffoldState> scaffoldkey = new GlobalKey<ScaffoldState>();

  HomeBottomNavBar1(this.catList, this.list);

  @override
  _HomeBottomNavBar1State createState() => _HomeBottomNavBar1State(catList);
}

class _HomeBottomNavBar1State extends State<HomeBottomNavBar1> {
  int _pageIndex = 0;
  bool floatingFlage = false;
  bool _proAppBar = true;
  var listFlag = 2;
  List<CatModel> catList0 = [];

  _HomeBottomNavBar1State(this.catList0);

  // static GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  final GlobalKey appBarKey = new GlobalKey();
  Widget currentScreen;

  // widget.currentTab = 0;
  PageStorageBucket bucket = PageStorageBucket();

  @override
  void initState() {
    HomeBottomNavBar1.currentTab = 0; // to keep track of active tab index

    currentScreen = MainPage(
        catList0); // MainPage(catList0); //Dashboard(); // Our first view in viewport
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: HomeBottomNavBar1.scaffoldkey,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.orange,
                Colors.orange,
                Colors.red,
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: AppBar(
            key: appBarKey,
            backgroundColor: Color.fromRGBO(255, 255, 255, 0),
            // title: _proAppBar ? Text('') : Text('Home'),
            centerTitle: true,
            actions: [
              HomeBottomNavBar1.currentTab == 0 ? SearchBar() : Container(),
              HomeBottomNavBar1.currentTab == 2
                  ? GestureDetector(
                      child: Icon(Icons.edit),
                    )
                  : Container(),
              HomeBottomNavBar1.currentTab == 0 && listFlag == 2
                  ? GestureDetector(
                      onTap: () {
                        setState(() {
                          floatingFlage = true;
                        });

                        var kiki = HomeBottomNavBar1.scaffoldkey.currentState
                            as ScaffoldState;
                        kiki
                            .showBottomSheet((context) {
                              return BottomSheetBuilder(Filter());
                            })
                            .closed
                            .then((value) {
                              setState(() {
                                floatingFlage = false;
                              });
                            });
                      },
                      child: Icon(
                        Icons.filter_alt,
                      ))
                  : Container(),
              Padding(padding: EdgeInsets.only(right: 20)),
            ],
          ),
        ),
      ),
      body: PageStorage(
        child: currentScreen,
        bucket: bucket,
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.orange,
        child: floatingFlage == false ? Icon(Icons.add) : Text('Apply'),
        onPressed: () {
          if (floatingFlage) {
            Navigator.pop(context);
            //in case of bottom sheet
          } else {
            var kiki =
                HomeBottomNavBar1.scaffoldkey.currentState as ScaffoldState;
            kiki.showBottomSheet((context) {
              return BottomSheetBuilder(ProductDetailsForm());
            }).closed;
            // in case adding add
          }
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    getNavBarItem([MainPage(catList0), ProductList()],
                        '  Home  ', Icons.home, 0),
                    getNavBarItem([ChatPage()], '    Chat    ', Icons.chat, 1),
                  ]),
              Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    getNavBarItem(
                        [ProfilePage()], 'Profile', Icons.account_circle, 2),
                    getNavBarItem(
                        [SettingsPage()], 'Setteings', Icons.settings, 3),
                  ]),
            ],
          ),
        ),
      ),
      drawer: sideDrawer(),
      endDrawer: Drawer(),
    );
  }

  Widget getNavBarItem(List<Widget> pages, title, IconData icon, colorIndex) {
    return MaterialButton(
      minWidth: 40,
      onPressed: () {
        setState(() {
          if (widget.catList.length == 0) {
            // if(catList0.le)
            currentScreen = ProductList();
          } else if (widget.catList.length != 0) {
            currentScreen = pages[0]; //MainPage(widget.catList);
          }

          HomeBottomNavBar1.currentTab = colorIndex;
        });
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              icon,
              color: HomeBottomNavBar1.currentTab == colorIndex
                  ? Colors.orange
                  : Colors.grey,
            ),
            Text(
              '$title',
              style: TextStyle(
                color: HomeBottomNavBar1.currentTab == colorIndex
                    ? Colors.orange
                    : Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
